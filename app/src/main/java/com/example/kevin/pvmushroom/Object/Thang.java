package com.example.kevin.pvmushroom.Object;

import android.content.Context;
import android.widget.Toast;

import com.example.kevin.pvmushroom.DanhSachStatic.DS_NAM;
import com.example.kevin.pvmushroom.Fragment.frag_of_PhieuThu.Tree_Node;
import com.example.kevin.pvmushroom.Fragment.hover.IconTreeItemHolder;
import com.example.kevin.pvmushroom.Fragment.hover.ProfileHolder;
import com.example.kevin.pvmushroom.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by kevin on 1/29/2016.
 */
public class Thang {
    int IDThangServer;

    public String getID_Code_Thang() {
        return ID_Code_Thang;
    }

    public void setID_Code_Thang(String ID_Code_Thang) {
        this.ID_Code_Thang = ID_Code_Thang;
    }

    String ID_Code_Thang;

    public Thang(int IDThangServer, Context context, String tenThang, ArrayList<Tuan> dsTuan, int tongTien, ParseObject data) {
        this.IDThangServer = IDThangServer;
        this.context = context;
        TenThang = tenThang;
        this.dsTuan = dsTuan;
        TongTien = tongTien;
        if(data == null)
        {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH,-cal.get(Calendar.DAY_OF_MONTH));
            cal.add(Calendar.DAY_OF_MONTH,1);
            ngayBD = cal.getTime();
        }
        else
            ngayBD = data.getDate("NGAY_BAT_DAU");
        this.data = data;
    }

    Context context;
    String TenThang;
    ArrayList<Tuan> dsTuan;
    int TongTien;
    Date ngayBD;
    ParseObject data;

    public int getIDServerVuMua() {
        return IDServerVuMua;
    }

    public void setIDServerVuMua(int IDServerVuMua) {
        this.IDServerVuMua = IDServerVuMua;
    }

    int IDServerVuMua;

    public Date getNgayBD() {
        return ngayBD;
    }

    public void setNgayBD(Date ngayBD) {
        this.ngayBD = ngayBD;
    }


    public ParseObject getData() {
        return data;
    }

    public void setData(ParseObject data) {
        this.data = data;
    }




    public String getTenThang() {
        return TenThang;
    }

    public void setTenThang(String tenThang) {
        TenThang = tenThang;
    }



    public ArrayList<Tuan> getDsTuan() {
        return dsTuan;
    }

    public void setDsTuan(ArrayList<Tuan> dsTuan) {
        this.dsTuan = dsTuan;
    }




    public void AddMoney(int Tien)
    {
        TongTien+=Tien;
    }

    public int getTongTien() {
        return TongTien;
    }

    public void setTongTien(int tongTien) {
        TongTien = tongTien;
    }

    public int getIDThangServer() {
        return IDThangServer;
    }

    public void setIDThangServer(int IDThangServer) {
        this.IDThangServer = IDThangServer;
    }
    public void KhoiTaoDSTuanCuaThang(int VuMua)
    {

        Calendar cal = Calendar.getInstance();
        cal.setTime(ngayBD);
        dsTuan = new ArrayList<>();

        for (int i =0; i < 4; i++)
        {
            Tuan tuanx = new Tuan(DS_NAM.generateIDLastWeek(),0,null,cal.getTime(),null);
            tuanx.setTenTuan(DS_NAM.getContext().getResources().getString(R.string.ds_tuan) + " " + (i + 1));
            cal.add(Calendar.WEEK_OF_MONTH, 1);
            tuanx.setNgayKT(cal.getTime());
            tuanx.CapNhatLenServer(ID_Code_Thang);
        }
    }

    public void KhoiTaoDSTuanCuaThang(List<ParseObject> list, Tree_Node root)
    {
        dsTuan = new ArrayList<>();
        for(int i = 0; i < list.size(); i++)
        {
            ParseObject pox = list.get(i);
            Tuan tuan = new Tuan(pox.getInt("ID"),pox.getInt("TONG_TIEN"),null,pox.getDate("NGAY_BAT_DAU"),pox.getDate("NGAY_KET_THUC"));
            tuan.setIDObjectOnServer(pox.getObjectId());
            tuan.setIDinListThang(i);
            tuan.setTenTuan(pox.getString("TEN_TUAN"));
            Tree_Node tuanx = new Tree_Node(new IconTreeItemHolder.IconTreeItem(R.string.ic_sd_storage, pox.getString("TEN_TUAN"), pox.getDate("NGAY_BAT_DAU"), pox.getDate("NGAY_KET_THUC"),tuan)).setViewHolder(new ProfileHolder(context.getApplicationContext()));
            tuan.setNodeTuan(tuanx);
            root.addChild(tuanx);
            dsTuan.add(tuan);
        }
    }
    public ParseObject CapNhatLenServer()
    {
        final ParseObject pox = new ParseObject("THANG");
        pox.put("ID", IDThangServer);
        pox.put("ID_VU_MUA", IDServerVuMua);
        pox.put("TONG_TIEN", TongTien);
        pox.put("TEN_THANG", TenThang);
        pox.put("NGAY_BAT_DAU", ngayBD);
        pox.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null) {
                    Toast.makeText(context, "Saved ! "+pox.getObjectId(), Toast.LENGTH_SHORT).show();
                    ID_Code_Thang = pox.getObjectId();
                }
                else
                    Toast.makeText(context,"Can't Save !",Toast.LENGTH_SHORT).show();

            }
        });
        return pox;
    }


}
